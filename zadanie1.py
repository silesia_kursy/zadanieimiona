#pobierz 3 ciagi znakowe i wyswietli długości tych znaków.
#Nie bierz pod uwagę whitespace'ów - na początku i końcu. Użyj do tego funkcji

#strip()

#x1 = input("Podaj x1: ")
#x2 = input("Podaj x2: ")
#x3 = input("Podaj x3: ")

lista = []

for element in range(0, 3):
    x = input("Podaj x: ")
    lista.append(x)

result = 0 
for element in lista:
    print(len(element.strip()))
    result += len(element.strip())
print("\n" +str(result))
